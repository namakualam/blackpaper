# config valid only for current version of Capistrano
lock '3.4.0'

set :application, "blackpaper"
set :repo_url, "git@bitbucket.org:namakualam/blackpaper.git"

set :deploy_to, "/var/www/#{fetch(:application)}"
set :deploy_user, "wave"

set :delayed_job_server_role, :worker
set :delayed_job_args, "-n 2"

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, "2.1.5"
set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value
set :format, :pretty
#set :log_level, :info

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

set :keep_releases, 5
