set :stage, :production
set :rails_env, :production

server "#{fetch(:deploy_user)}@128.199.80.219:7689", roles: %w{web app db}, primary: true