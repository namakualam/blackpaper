bash "swap" do
  code <<-EOH
    fallocate -l 4G /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
    echo '/swapfile   none    swap    sw    0   0' >>  /etc/fstab
  EOH
  not_if "grep -xq '/swapfile   none    swap    sw    0   0' /etc/fstab"
end