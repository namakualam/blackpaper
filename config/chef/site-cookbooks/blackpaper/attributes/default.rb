default['app']               = 'blackpaper'

default['nodejs']['dir']     = '/usr/local'
default['nodejs']['version'] = '0.10.32'

default['ruby']['version']   = '2.1.5'
default['redis']['version']  = '2.8.13'