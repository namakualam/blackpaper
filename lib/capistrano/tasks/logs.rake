namespace :logs do
  desc "tail rails logs"
  task :production do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/production.log"
    end
  end
  task :unicorn do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/unicorn.log"
    end
  end
end