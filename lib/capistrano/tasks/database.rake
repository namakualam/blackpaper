namespace :reset do
  desc "tail rails logs"
  task :drop do
    on roles(:app) do
      execute "/etc/init.d/unicorn_#{fetch(:application)} stop"
      within "#{current_path}" do
        with rails_env: :production do
          execute :rake, "db:drop"
        end
      end
    end
  end
  task :seed do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: :production do
          execute :rake, "db:seed"
        end
      end
    end
  end
  task :migrate do
    on roles(:app) do
      within "#{current_path}" do
        with rails_env: :production do
          execute :rake, "db:migrate"
        end
      end
    end
  end
end